


	var jungleCaption = 
		[
		"Full Moon Pre-party at the Jungle Experience",
		"Jungle Experience - Full Moon Warm-Up Party",
		"Jungle Experience Party - One day before the Full Moon festival",
		"Jungle Experience Koh Phangan, one day before the Full Moon Party",
		"Fullmoon WarmUp party at the Jungle Experience"
		]
		
		
		
	var dropinCaption = 
		[
		"Full Moon Party at Drop In Bar",
		"Drop In Bar full Moon Party",
		"Drop In Bar Full Moon night",
		"Celebrating the Full Moon party at Drop In Bar",
		"Fullmoon at Drop In Bar, Koh Phangan",
		"Full Moon party night at Drop In Bar"
		]
		
	var waterfallcaption = 
		[
		"Waterfall party Koh Phangan",
		"Waterfall Party - Two days before the Full Moon Party",
		"Waterfall Party, Full Moon Party Warm-Up, two days before the Full Moon festival",
		"Waterfall Party",
		"Waterfall Party - Koh Phangan"
		]
		
		
	var leoCaption =
		[
		"Leo Full Moon Party at Paradaise",
		"Full Moon Party at Paradise by Leo",
		"Leo Full Moon on Haad rin beach, Koh Phangan",
		"Celebrating with Leo at the Full Moon Party",
		"Full Moon night with Leo",
		"Long party night with Leo at the Full Moon Party"
		]
		
	var backyardCaption = 
		[
		"Full Moon afterparty at Backyard",
		"Backyard party - Full Moon afterparty on Koh Phangan",
		"Afterparty at Backyard"
		]


// need to use only one style of export so nodejs will not go crazy =)

// old style
//module.exports = ......what will be exported

// module.exports = backyardCaption
// so from other file
// let xxx = require('./path/to/this/file')
// will become
// now xxx === backyardCaption

module.exports = {
	backyard: backyardCaption,
	leo: leoCaption,
	jungle: jungleCaption,
	waterfall: waterfallcaption,
	dropin: dropinCaption
}

//from other file
let captions = require('./path/to/this/file')
//and we will get
//captions.backyard now holds backyardCaption array
//captions.leo === leoCaption
////......



// new ES6 style
//export backyardCaption

// so from other file
// let xxx = require('./path/to/this/file').backyardCaption
// will become
// now xxx === backyardCaption

// new ES6 style littile different (look at default keyword)
//export default backyardCaption

// so from other file
// let xxx = require('./path/to/this/file')
// will become
// now xxx === backyardCaption

