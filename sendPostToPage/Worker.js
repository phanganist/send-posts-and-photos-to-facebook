var axios = require('axios')
var FB = require('fb');
var fs = require('fs')
var http = require('http');
const path = require('path');
var ExifImage = require('exif').ExifImage;
var moment = require ('moment')
var urllib = require('urllib');

const Telegraf = require('telegraf')
const Extra = require('telegraf/extra')



class Worker
{
	constructor()
	{
		this.token = null
		this._tgToken = null
		this.photoToPost = null
		
		this._tgadmin = null
		
		this._bot = null
		
		this._lastAlbum = null
		
		//elements for photo caption from image metadata and view gallery owner
		this._photoCreateDate = null
		this._photoGalleryOwner = null
		this._photoCaption = null
		
		
		//elements for post publishing
		this._postUrl = null
		this._postBody = null
		this._postYoutubeUrl = null
		this._postTitle = null
		this._lastPost = null

	}
	
	
	init(fbToken,tgToken,tgAdmin)
	{
		this.token = fbToken
		this._tgToken = tgToken
		this._tgadmin = tgAdmin
		
		this._bot =  new Telegraf(this._tgToken)
		
		
		
		FB.setAccessToken(this.token)
	}


	api(endpoint,method,body)
	{
		return new Promise((acc,rej)=>{
			FB.api(enpoint, method, body, function (res) {
			  if(!res || res.error) {
				console.log('error occurred', res);
				rej(res)
				return;
			  }
			  acc(res)
			});
		})
	}


	async getPostFromView()
	{
			
		
		let result = await axios.get('https://phanganist.com/api/1.0/articles/random')
	
		//data from result	
		let postNumber = Math.floor(Math.random() * 30)
			console.log('album number is   ',postNumber)
		let data = result.data.nodes[postNumber].node
	
		
		//album information is encrypted JSON string
		//restore to object
		//let post = JSON.parse(data.album)
		this._lastPost = data
		//this._photoGalleryOwner = data.galleryOwner
		
		console.log ('album  is     ',data)
		console.log('album title is    ',data.title)

		
	
		//elements for post publishing
		this._postUrl = 'https://phanganist.com'+data.shareUrl
		
		
		
		
		//this._postBody = data.body
		
		
		this._postBody  = '\n \nHi! My name is \nJenya Sotnik\nI was born in the Far East of Russia,but last 4 years I lived in Moscow'
		
		
		
		//let tmp = this._postBody.replace('\n',' ')
		//this._postBody = tmp.trim()
		//console.log('body after trimming is  ',this._postBody)
			
		this._postYoutubeUrl = data.youtube
		this._postTitle = data.title		
		this._postDate = moment(data.datetime,'YYYY-MM-DD HH:mm').format('MMMM YYYY')
		console.log ('post date only month and year     ',this._postDate)
		this._lastPost = this._postBody+'\n'+this._postDate
		
		console.log('The values of this._postUrl    ',this._postUrl )
		console.log('The values of this._postBody    ',this._postBody )
		console.log('The values of this._postYoutubeUrl    ',this._postYoutubeUrl )
		console.log('The values of this._postTitle    ',this._postTitle )
		console.log('The values of this._postDate    ',this._postDate )
		console.log('The values of this._lastPost    ',this._lastPost )

		
	}


	 
	
	
	//handing all access token processes starting with getting short user token specific for the required App from Facebook and base parameter
	async authenticateElemnts()
	{
	
		//Setting all basic parameters required, taken from facebook developer
		
		let userID = '10157421755333316'
		let appId = '528845507291143'
		let appSecret = '3c5b864e30f8d1d21481f29425f6c3b0'
		let pageID = '263769874049324' // Phanganist Lounge Bar
		//let pageID = '107199927429971' // Full Moon party Koh Phangan FB Page
		//let pageID = '217617728269483' // Koh Phangan party Daily Update FB Page
		let TK = 'EAAHgZB3CaaAcBAInf04UgASiD6R1L01PLQGZC3xpZCeJHPRQcP3zc0RBJzWbPoRsEtUvLaZB3BAETFteONzj3s6D7C952rBHCaFIZB70BZAt4YmLnfRbcZAEqVW2JwkRK2uiRxz6w0dsFtPRD6BULhR1WTPTy4jwhPLxnvRYSCq6904ZBfJZCh1CN'
		FB.setAccessToken(TK)
		
			
		
		// Getting long Access Token
		let pageAccessToken = await FB.api('oauth/access_token', {grant_type:'fb_exchange_token',client_id: appId,client_secret: appSecret,fb_exchange_token: TK})
		var longAT = pageAccessToken.access_token
		console.log('this is long access token',longAT)
			
		
		
		// Getting Page Access Token
		let pageTokenToken = await FB.api(pageID,{fields: 'access_token', access_token: longAT})
		console.log('pagetoken display  ',pageTokenToken.access_token)
		let pageAcessToken = pageTokenToken.access_token
		return pageTokenToken.access_token
		
	
	}
	
	
	
	
    async postToFacebook(AcessToeknForPage) 
	{
		
		// Set the long access token into facebook instead of the short one - not sure if needed , maybe already was inserted in prev processes
		FB.setAccessToken(AcessToeknForPage)
		
		if (this._postBody!=null)
		{	
			if (this._postYoutubeUrl!=null)
			{
				
				let linkToShare = 'https://www.youtube.com/watch?v='+this._postYoutubeUrl
				console.log ('the link to shae is   ', linkToShare)
				let sendPostToFacebook = await FB.api('me/feed', 'post', { link: linkToShare, message: this._lastPost })
				console.log('post to faceboo was sent',sendPostToFacebook)
				
			}
			else 
			{
				let sendPostToFacebook = await FB.api('me/feed', 'post', { link: this._postUrl, message: this._lastPost })
				console.log('post to faceboo was sent',sendPostToFacebook)
			}
			
		}
		else console.log ('body value is null')
				
	}
		
	

	
	async letsGo()
	 
	
	{	
		this._bot.launch()
		let postDataRecicieved = await this.getPostFromView()
		let pageAccessTokenData = await this.authenticateElemnts() 
		
		//send post to facebook
		await this.postToFacebook(pageAccessTokenData)
		
		//now post it to FB with api method
//		await this.api('.....','post',{photo:photo})
		this._bot.stop()
	}
	
	
	
	
	
	/*choosePhoto()
	{
		//
		return ''
	}
	
	myAppStartHere()
	{
		//choose photo
		let photoUrl = this.choosePhoto()
		//download it
		//configure fb
		//upload to fb
		//clear temp file
	}
	
	*/
}

module.exports = Worker  


