/*
// Using require() in ES5
var FB = require('fb');
 
// Using require() in ES2015
var {FB, FacebookApiException} = require('fb');
 
// Using ES2015 import through Babel
import FB from 'fb'; // or,
import {FB, FacebookApiException} from 'fb';

*/

/*
function downloadFile(url,callbackFunction)
{
	console.log(2)
	setTimeout(function(){
		console.log(4)
		var result = 'this is my file from url ' + url
		//return result
		callbackFunction(result)
	},2000)
	console.log(3)
}


console.log(1)

var file = downloadFile('.....',function(fileContent){
	console.log(5)

	console.log('my file is',fileContent)	
})

*/

/*

//  callbacks

function a(arg) {
	console.log('f a',arg)
}

function b(arg,f)
{
	console.log('f b',1)
	f(arg)
	console.log('f b',2)
	f(arg)
}

function c(arg, fEveryTime, fErr, fAllDone)
{
	console.log('f b',1)
	fEveryTime(arg)
	console.log('f b',2)
	fEveryTime(arg)
	// if i have some error happens
	//i call
	//fErr(error)
	fAllDone()
}



c(1,a,function(){},function(){
	console.log('all done')
})

*/


/*
// classes and lambdas

var myFunc = function() {}
var myFunc2 = ()=>{}

// this is DEFENITION
class A {
	

	constructor(p)
	{
		console.log('constructor called')
		this.someParametr = p
	}
	
	callMe() {
		console.log('i was called from A class', this.someParametr)
	}
	
	callMeWithCallback(waitForSecs,cb)
	{
		setTimeout(()=>{
			this.callMe()
			cb()
		},1000*waitForSecs)
	}
}

// creation of object, which is described by class
var a = new A(1)

a.callMeWithCallback( 3, function(){
	console.log('ok from callback, this is: ',this)
} )
*/

/*
()=>{}

(arg)=>{}

(_)=>{}


_=>{}

(arg1,arg2)=>{}
*/


// callback hell

//emulate for some long process
function waitFor(sec,cb)
{
	setTimeout(_=>{
		cb(sec)
	},sec*1000)
}

function getUrlData(url,cb)
{
	waitFor(2,_=>{
		cb(_)
	})
}

function manageGrabbedData(data,cb)
{
	waitFor(2,cb)
}

function manageMore(data,cb)
{
	waitFor(2,cb)
}

/*
function myScenario()
{
	var urlToGrab = '....'
	getUrlData(urlToGrab,urlData=>{
		manageGrabbedData(urlData,managedResult=>{
			manageMore(managedResult,res2=>{
				/// and we go more deep so this is our CALLBACK HELL
				// only here i have some RESULTS
			})
		})
	})
}
*/

// Promises are fix callback hell somehow

function waitForPromised(sec)
{
	return new Promise((accept,reject)=>{
		// .... do somethig long
		accept(sec)
	})
}

function getUrlDataPromised(url)
{
	return waitForPromised(2)
}

/*
waitForPromised(2).then(result=>{
	console.log('result is: ',result)
})

getUrlDataPromised('...').then(res=>{
	console.log('result get data is:', res)
})


getUrlDataPromised('...').then(res=>{
	console.log('result get data is:', res)
	waitForPromised(2).then(result=>{
		console.log('result is: ',result)
	})
})
*/

async function fff()
{
	var waitForRes = await waitForPromised(2)
	console.log('my wait is: ',waitForRes)
	var grabbedData = await getUrlDataPromised('....')
	console.log('my grabbed data is:', grabbedData)
}


fff().then(res=>{
	console.log('all async done')
})

class Loader
{
	async grabUrl(url)
	{
		//.... await
	}
	
}

class Grabber
{
	constructor()
	{
		this.loader = new Loader()
	}
	
	async grabUrl(url)
	{
		return await this.loader.grabUrl('...')
	}
	
	async parseData(data)
	{
		//..... await
	}
	
	async doAll()
	{
		let data = await this.grabUrl('...')
		let parsed = await this.parseData(data)
		return parsed
	}
}


var myObj = new Grabber()

myObj.doAll().then(res=>{
	///// 
})



function X()
{
	var i = 1
	console.log('before loop i is:',i)
	for(var i=0;i<5;i++)
	{
		console.log('loop i is:',i)
	}
	console.log('after loop i is:',i)
}

X()

// Reactive programming uses RxJS library