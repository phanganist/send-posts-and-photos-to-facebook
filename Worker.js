var axios = require('axios')
var FB = require('fb');
var fs = require('fs')
var http = require('http');
const path = require('path');
var ExifImage = require('exif').ExifImage;
var moment = require ('moment')

const Telegraf = require('telegraf')
const Extra = require('telegraf/extra')



class Worker
{
	constructor()
	{
		this.token = null
		this._tgToken = null
		this.photoToPost = null
		
		this._tgadmin = null
		
		this._bot = null
		
		this._lastAlbum = null
		
		//elements for photo caption from image metadata and view gallery owner
		this._photoCreateDate = null
		this._photoGalleryOwner = null
		this._photoCaption = null
		
		
	}
	
	
	init(fbToken,tgToken,tgAdmin)
	{
		this.token = fbToken
		this._tgToken = tgToken
		this._tgadmin = tgAdmin
		
		this._bot =  new Telegraf(this._tgToken)
		
		
		
		FB.setAccessToken(this.token)
	}
	
	api(endpoint,method,body)
	{
		return new Promise((acc,rej)=>{
			FB.api(enpoint, method, body, function (res) {
			  if(!res || res.error) {
				console.log('error occurred', res);
				rej(res)
				return;
			  }
			  acc(res)
			});
		})
	}
	
	async getPhoto()
	{
		let result = await axios.get('https://phanganist.com/api/1.1/galleries/random')
		console.log('this is result          ',result.data.nodes[1].node)

		//data from result	
		let albumNumber = Math.floor(Math.random() * 3)
			console.log('album number is   ',albumNumber)
		let data = result.data.nodes[albumNumber].node
	
		
		//album information is encrypted JSON string
		//restore to object
		let album = JSON.parse(data.album)
		this._lastAlbum = data
		this._photoGalleryOwner = data.galleryOwner
		
		console.log ('album  is     ',data.galleryOwner)



		//grab images urls from album
		//with replacement to origirnal size url
		
		let thumbs = album.map(x=>{
			let url = x.ThumbnailUrl
			url = url.replace('/Th/','/O/')
			url = url.replace('-Th.','-O.')
			return url
		})
		
		//get random number from 0 to images size
		let rnd = Math.floor(Math.random()*thumbs.length)
		let img = thumbs[rnd]
		
//		console.log('i want to publish this image: ',img)
		this.photoToPost = img
		console.log ('image value is   ',img)
		
		
		return img
	}

	async downloadPhoto(url,saveAs)
	{
		const writer = fs.createWriteStream(saveAs)

		const response = await axios({
			url,
			method: 'GET',
			responseType: 'stream'
		})

		response.data.pipe(writer)

		return new Promise((resolve, reject) => {
			writer.on('finish', resolve )
			writer.on('error', reject)
		})
		
	}

	
	
	
	
	//handing all access token processes starting with getting short user token specific for the required App from Facebook and base parameter
	async authenticateElemnts()
	{
	
		//Setting all basic parameters required, taken from facebook developer
		
		let userID = '10157421755333316'
		let appId = '528845507291143'
		let appSecret = '3c5b864e30f8d1d21481f29425f6c3b0'
		//let pageID = '263769874049324' // Phanganist Lounge Bar
		let pageID = '217617728269483' // KOh Phangan Party List daily Update
		let TK = 'EAAHgZB3CaaAcBAInf04UgASiD6R1L01PLQGZC3xpZCeJHPRQcP3zc0RBJzWbPoRsEtUvLaZB3BAETFteONzj3s6D7C952rBHCaFIZB70BZAt4YmLnfRbcZAEqVW2JwkRK2uiRxz6w0dsFtPRD6BULhR1WTPTy4jwhPLxnvRYSCq6904ZBfJZCh1CN'
		FB.setAccessToken(TK)
		
			
		
		// Getting long Access Token
		let pageAccessToken = await FB.api('oauth/access_token', {grant_type:'fb_exchange_token',client_id: appId,client_secret: appSecret,fb_exchange_token: TK})
		var longAT = pageAccessToken.access_token
		console.log('this is long access token',longAT)
			
		
		
		// Getting Page Access Token
		let pageTokenToken = await FB.api(pageID,{fields: 'access_token', access_token: longAT})
		console.log('pagetoken display  ',pageTokenToken.access_token)
		let pageAcessToken = pageTokenToken.access_token
		return pageTokenToken.access_token
		
	
	}
	
	
	
	async uploadPhoto(AcessToeknForPage,tempFile,caption) {
	
	
		
		console.log ('checking AcessToeknForPage velaue before calling the function', AcessToeknForPage)
		// Set the long access token into facebook instead of the short one - not sure if needed , maybe already was inserted in prev processes
		FB.setAccessToken(AcessToeknForPage)


		// Post to facebook Page
		
		//get caption
		let photoCaption = caption
		console.log('this is caption inside function upload photo    ',photoCaption)
		
		
		//send request to upload the photo to facebook page
		//let uploadPhotoToFacebook = await FB.api('me/photos', 'post', { source: fs.createReadStream(tempFile), caption: photoCaption })
		console.log('api res',uploadPhotoToFacebook)
	
	
	}
	
	checkPhotoEXIF(filePath)
	{
		return new Promise((acc,rej)=>{
			new ExifImage({image:filePath}, (error, exifData)=>{
				if (error)
				{
					console.log('Error: '+error.message);
					acc(null)
					return
				}
//				console.log(exifData);
				acc(exifData)
			});
		})
	}
	
	
	async letsGo()
	{
		console.log('hello hello')
		this._bot.launch()
		
		let okPhotoFound = false
		
		while(!okPhotoFound)
		{
			let photoUrl = await this.getPhoto()
			
			if (!this._photoGalleryOwner)
			{
				continue
			}
			
			console.log('got photo url',photoUrl)
			//let tempFile = './photo.jpg'
			var tempFile = photoUrl.split('/').pop()
			
			await this.downloadPhoto(photoUrl,tempFile)
			//checking EXIF data of photo
			let exifData = await this.checkPhotoEXIF(tempFile)
			
			console.log(Object.keys(this._lastAlbum))
			//console.log('gallery owner is    ',this._lastAlbum.gallery-owner)
			let caption = JSON.stringify({
				album:this._lastAlbum.title,
				albumNID:this._lastAlbum.nid
			})

			//this._bot.launch()
			//await this._bot.telegram.sendPhoto(this._tgadmin,{source:tempFile},Extra.caption(caption).markdown())
			//this._bot.stop()

			if (exifData!==null)
			{
				okPhotoFound = true
				//call function to gran the date created field
				let photoCreateDate = exifData.exif.CreateDate
				console.log('date is     ',photoCreateDate)
				//let imageCaption = await this.getCaptionData(exifData)
				var photoCreateDateMoment = moment(photoCreateDate,'YYYY:MM:DD HH:mm:ss').format('MMMM YYYY')
				this._photoCreateDate = photoCreateDateMoment
				console.log('moment value  ',photoCreateDateMoment)
				
				
			} else {
				//if EXIT not exists - download new, and send information about current to admin
				//sending photo to tg of admin
				await this._bot.telegram.sendPhoto(this._tgadmin,{source:tempFile},Extra.caption(caption).markdown())
				fs.unlinkSync(tempFile)
			}
			
		}

		console.log('saved photo')
		let pageAccessTokenData = await this.authenticateElemnts() 
		console.log('Page Acess token achieved')
		console.log('tempFile value    ',tempFile)
		console.log('moment value inside lets go  ',photoCreateDateMoment)
		this._photoCaption = this._photoGalleryOwner+" - "+this._photoCreateDate
		console.log('this._photoCaption value is   ',this._photoCaption)
		
		//await this.uploadPhoto(pageAccessTokenData,tempFile,this._photoCaption)
		console.log('uploaded photo')
		fs.unlinkSync(tempFile)
		console.log('temporary file removed')
		//now post it to FB with api method
//		await this.api('.....','post',{photo:photo})
		this._bot.stop()
	}
	
	/*choosePhoto()
	{
		//
		return ''
	}
	
	myAppStartHere()
	{
		//choose photo
		let photoUrl = this.choosePhoto()
		//download it
		//configure fb
		//upload to fb
		//clear temp file
	}
	
	*/
}

module.exports = Worker  


